$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/store.feature");
formatter.feature({
  "name": "Adidas online shopping portal",
  "description": "",
  "keyword": "Feature"
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User navigate to store homepage",
  "keyword": "Given "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_navigate_to_store_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Customer navigation to Laptop category",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User clicks on \"Laptop\" category",
  "keyword": "When "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on_category(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"Sony vaio i5\"",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User adds to \"Add to cart\"",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_adds_to(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accepts pop up confirmation",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_accepts_pop_up_confirmation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to homepage",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_navigates_to_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"Laptop\" category",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on_category(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"Dell i7 8gb\"",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on \"Add to cart\"",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accepts pop up confirmation",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_accepts_pop_up_confirmation()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to cart",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_navigates_to_cart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User delete \"Dell i7 8gb\" from cart",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_delete_from_cart(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User places order",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_places_order()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User fill in web form details",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_fill_in_web_form_details()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click on purchase",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_clicks_on_purchase()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Capture Purchase Id and Amount",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.capture_Purchase_Id_and_Amount()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Assert Purchase Amount Equals Expected",
  "keyword": "Then "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.assert_Purchase_Amount_Equals_Expected()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User accepts on \"OK\"",
  "keyword": "And "
});
formatter.match({
  "location": "com.qa.store.StorePageActions.user_accepts_on(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});