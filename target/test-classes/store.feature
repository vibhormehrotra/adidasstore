Feature: Adidas online shopping portal

  Background: 
    Given User navigate to store homepage

  Scenario: Customer navigation to Laptop category
    When User clicks on "Laptop" category
    And User clicks on "Sony vaio i5"
    And User adds to "Add to cart"
    And User accepts pop up confirmation
    And User navigates to homepage
    And User clicks on "Laptop" category
    And User clicks on "Dell i7 8gb"
    And User clicks on "Add to cart"
    And User accepts pop up confirmation
    And User navigates to cart
    And User delete "Dell i7 8gb" from cart
    And User places order
    And User fill in web form details
    And User click on purchase
    And Capture Purchase Id and Amount
    Then Assert Purchase Amount Equals Expected
    And User accepts on "OK"