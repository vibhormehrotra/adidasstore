package com.qa.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class WebDriverFactory {

	public WebDriver driver;

	/**
	 * This method is used to start the browser.
	 * 
	 * @return
	 */
	public WebDriver startBrowser(String browser) {

		try {
			if (browser.equalsIgnoreCase("firefox") || browser.equalsIgnoreCase("ff")) {
				System.setProperty("webdriver.gecko.driver", "drivers" + File.separator + "geckodriver.exe");
				DesiredCapabilities capabilities = DesiredCapabilities.firefox();
				capabilities.setCapability("marionette",true);
				driver = new FirefoxDriver(capabilities);
			} else if (browser.equalsIgnoreCase("chrome") || browser.equalsIgnoreCase("googlechrome")) {
				if (System.getProperty("os.name").contains("Linux")) {
					System.setProperty("webdriver.chrome.driver", "drivers" + File.separator + "chromedriver");
				} else {
					System.setProperty("webdriver.chrome.driver", "drivers" + File.separator + "chromedriver.exe");
				}
				ChromeOptions options = new ChromeOptions();
				Map<String, Object> prefs = new HashMap<String, Object>();
				options.setExperimentalOption("prefs", prefs);
				options.addArguments("ignore-certificate-errors");
				options.addArguments("allow-running-insecure-content");
				options.addArguments("--test-type");
				options.addArguments("disable-infobars");
				options.addArguments("disable-extensions");
				options.addArguments("chrome.switches", "--disable-extensions");
				options.setExperimentalOption("useAutomationExtension", false);
				driver = new ChromeDriver(options);

			} else if (browser.equalsIgnoreCase("internetexplorer") || browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver", "drivers" + File.separator + "IEDriverServer.exe");
				DesiredCapabilities capability = new DesiredCapabilities();
				capability.setJavascriptEnabled(true);
				capability.setCapability("ignoreProtectedModeSettings", true);
				capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

				driver = new InternetExplorerDriver(capability);

			} else if (browser.isEmpty()) {
				driver = new FirefoxDriver();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return driver;
	}
}
