package com.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SeleniumWait extends WebDriverFactory {
	
	static Integer MAX_WAIT = 60;
	
	public static boolean waitForElementPresent(WebDriver driver,final WebElement webElement, int timeOut) {
    	WebDriverWait wait = new WebDriverWait(driver, timeOut/1000); // in seconds
    	boolean result;
    	try {
    		result = wait.until(ExpectedConditions.visibilityOf(webElement)) != null ? true:false;
    	} catch(org.openqa.selenium.TimeoutException e) {
    		result = false;
    	}
        return result;
    }
	
	public static boolean waitForElementPresent(WebDriver driver, WebElement webElement) {
        return waitForElementPresent(driver, webElement, MAX_WAIT);
    }

}
