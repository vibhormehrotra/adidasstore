package com.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CommonActions extends SeleniumWait {

	public static void click(WebDriver driver, WebElement webElement) {	
		waitForElementPresent(driver, webElement);
		webElement.click();
	}
	
	public static void clickWithText(WebDriver driver, String value) {	
		waitForElementPresent(driver, driver.findElement(By.xpath("//a[text()='" + value + "']")));
		driver.findElement(By.xpath("//a[text()='" + value + "']")).click();
	}

	public static void enterText(WebDriver driver, WebElement webElement,String text) {	
		waitForElementPresent(driver, webElement);
		webElement.sendKeys(text);
	}
	
	public static void acceptPopUp(WebDriver driver) {
		driver.switchTo().alert().accept();
	}
	
	public static void navigateToURL(WebDriver driver, String url) {
		driver.get(url);
		driver.manage().window().maximize();
	}
}
