package com.qa.store;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/html/", "json:target/json/testResults.json" }, features = "src/test/resources")
public class RunnerTest {

}
