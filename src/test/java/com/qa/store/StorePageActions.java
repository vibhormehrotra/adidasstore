package com.qa.store;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.qa.utils.CommonActions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StorePageActions extends CommonActions {

	WebDriver driver;
	String total_order,expectedAmount,actualAmount = null;

	@Before
	public void tearSetUp() {
		driver = startBrowser("chrome");
	}

	@After
	public void tearSetupDown() {
		driver.quit();
	}

	@Given("User navigate to store homepage")
	public void user_navigate_to_store_homepage() {
		navigateToURL(driver, "https://www.demoblaze.com/index.html");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	}

	@When("^User clicks on \"(.*?)\" category$")
	public void user_clicks_on_category(String category) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[@id='cat']/following-sibling::a[2]")).click();
		Thread.sleep(3000);
	}

	@When("^User clicks on \"(.*?)\"$")
	public void user_clicks_on(String value) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[text()='" + value + "']")).click();
		Thread.sleep(3000);
	}

	@When("^User adds to \"(.*?)\"$")
	public void user_adds_to(String value) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[text()='" + value + "']")).click();
		Thread.sleep(3000);
	}

	@When("User accepts pop up confirmation")
	public void user_accepts_pop_up_confirmation() {
		acceptPopUp(driver);
	}

	@When("User navigates to homepage")
	public void user_navigates_to_homepage() {
		navigateToURL(driver, "https://www.demoblaze.com/index.html");
	}
	
	@When("User navigates to cart")
	public void user_navigates_to_cart() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//a[text()='Cart']")).click();
		Thread.sleep(3000);
	}

	@When("User delete {string} from cart")
	public void user_delete_from_cart(String delete) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//td[text()='" + delete + "']/following-sibling::td//a")).click();
		Thread.sleep(3000);
		expectedAmount = driver.findElement(By.id("totalp")).getText();
	}
	
	@When("User places order")
	public void user_places_order() throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='Place Order']")).click();
		Thread.sleep(1000);
	}

	@When("User fill in web form details")
	public void user_fill_in_web_form_details() throws InterruptedException {
		Thread.sleep(2000);
		enterText(driver, driver.findElement(By.id("name")), "name "+System.currentTimeMillis());
		enterText(driver, driver.findElement(By.id("country")), "India");
		enterText(driver, driver.findElement(By.id("city")), "Noida");
		enterText(driver, driver.findElement(By.id("card")), "1234");
		enterText(driver, driver.findElement(By.id("month")), "12");
		enterText(driver, driver.findElement(By.id("year")), "2020");
	}
	
	@When("^User click on purchase$")
	public void user_clicks_on_purchase() throws InterruptedException {
		Thread.sleep(3000);
	    driver.findElement(By.xpath("//button[text()='Purchase']")).click();
		Thread.sleep(3000);
	}

	@When("Capture Purchase Id and Amount")
	public void capture_Purchase_Id_and_Amount() {
	    String value = driver.findElement(By.xpath("//h2/following-sibling::p")).getText();
	    String[] purchase_details = value.split("\n");
	    System.out.println("Purchase Id : "+purchase_details[0].split(":")[1].trim());
	    System.out.println("Actual Amount : "+purchase_details[1].split(":")[1].trim().split(" ")[0]);
	    actualAmount = purchase_details[1].split(":")[1].trim().split(" ")[0];
	}

	@Then("Assert Purchase Amount Equals Expected")
	public void assert_Purchase_Amount_Equals_Expected() {
	    Assert.assertEquals(expectedAmount, actualAmount);
	}
	
	@Then("^User accepts on \"(.*?)\"$")
	public void user_accepts_on(String value) throws InterruptedException {
		Thread.sleep(3000);
		driver.findElement(By.xpath("//button[text()='" + value + "']")).click();
		Thread.sleep(3000);
	}

}
